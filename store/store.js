import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import thunk from "redux-thunk";
import {rootReducer} from "./reducers/rootReducer";

const mainReducer = combineReducers({
    root: rootReducer
})

const store = createStore(mainReducer,compose(
    applyMiddleware(
        thunk
    ),
))

export default store