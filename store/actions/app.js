import axios from "axios";
import {
    CHOOSE_PROJECT,
    FETCH_CATEGORIES, FETCH_PROJECT_CATEGORIES,
    FETCH_PROJECTS,
    FETCH_SERVICE,
    FETCH_SETTINGS, FETCH_SOLUTIONS,
    FETCH_TECHNOLOGIES, FETCH_WORKERS
} from "../types";

export const serverApi = `https://backs.it-lead.net/api`
import Swal from "sweetalert2";

export function fetchProjects() {
    return async dispatch => {
        axios.get(`${serverApi}/projects`)
            .then(response => dispatch({type: FETCH_PROJECTS, payload: response.data.data}))
            .catch(e => console.log(e))
    }
}

export function fetchProject(id) {
    return async dispatch => {
        axios.get(`${serverApi}/projects/${id}`)
            .then(response => dispatch({type: CHOOSE_PROJECT, payload: response.data.data}))
            .catch(e => console.log(e))
    }
}

export function getTechnologies() {
    return async dispatch => {
        axios.get(`${serverApi}/technologies`)
            .then(response => dispatch({type: FETCH_TECHNOLOGIES, payload: response.data.data}))
            .catch(e => console.log(e))
    }
}

export function fetchSettings() {
    return async dispatch => {
        axios.get(`${serverApi}/settings`)
            .then(response => dispatch({type: FETCH_SETTINGS, payload: response.data}))
            .catch(e => console.log(e))
    }
}

export function sendApplication(form) {
    return async dispatch => {
        axios.post(`${serverApi}/applications`, form)
            .then(response => Swal.fire({
                title: "Успешно",
                text: response.message,
                icon: 'success',
                confirmButtonText: 'Хорошо'
            }))
            .catch(e => {
                let errors = []
                console.log(e.response.data)
                for (let error in e.response.data.errors) {
                    errors.push(e.response.data.errors[error][0])
                }

                Swal.fire({
                    title: "Ошибка",
                    text: errors.join('\n'),
                    icon: 'error',
                    confirmButtonText: "Ок"
                })
            })
    }
}

export function fetchCategories() {
    return async dispatch => {
        axios.get(`${serverApi}/services`)
            .then(response => dispatch({type: FETCH_CATEGORIES, payload: response.data.data}))
            .catch(e => console.log(e))
    }
}

export function fetchService(slug) {
    return async dispatch => {
        axios.get(`${serverApi}/services/${slug}`)
            .then(response => dispatch({type: FETCH_SERVICE, payload: response.data.data}))
            .catch(e => console.log(e))
    }
}

export function fetchWorkers() {
    return async dispatch => {
        axios.get(`${serverApi}/workers`)
            .then(response => dispatch({type: FETCH_WORKERS, payload: response.data.data}))
            .catch(e => console.log(e))
    }
}

export function fetchProjectCategories() {
    return async dispatch => {
        axios.get(`${serverApi}/categories`)
            .then(response =>
                dispatch({type: FETCH_PROJECT_CATEGORIES, payload: response.data.data})
            )
            .catch(e => console.log(e))
    }
}

export function fetchSolutions(){
    return async dispatch=>{
        axios.get(`${serverApi}/solutions`)
            .then(response=>dispatch({type:FETCH_SOLUTIONS,payload:response.data}))
            .catch(e=>console.log(e))
    }
}