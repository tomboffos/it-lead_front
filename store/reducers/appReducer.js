import {
    CHOOSE_PROJECT,
    FETCH_CATEGORIES, FETCH_PROJECT_CATEGORIES,
    FETCH_PROJECTS,
    FETCH_SERVICE,
    FETCH_SETTINGS, FETCH_SOLUTIONS,
    FETCH_TECHNOLOGIES, FETCH_WORKERS
} from "../types";

const initialState = {
    projects : [],
    project:{},
    technologies:[],
    settings:{},
    categories:[],
    service:{},
    workers:[],
    projectCategories: [],
    fastSolutions: []
}

export const appReducer = (state = initialState,action)=>{
    switch (action.type){
        case FETCH_PROJECTS:
            return {...state,projects:action.payload}
        case CHOOSE_PROJECT:
            return {...state,project: action.payload}
        case FETCH_TECHNOLOGIES:
            return {...state,technologies: action.payload}
        case FETCH_SETTINGS:
            return {...state,settings: action.payload}
        case FETCH_CATEGORIES:
            return {...state,categories: action.payload}
        case FETCH_SERVICE:
            return {...state,service: action.payload}
        case FETCH_WORKERS:
            return {...state,workers:action.payload}
        case FETCH_PROJECT_CATEGORIES:
            return {...state,projectCategories: action.payload}
        case FETCH_SOLUTIONS:
            return {...state,fastSolutions: action.payload}
        default:
            return  state
    }
}