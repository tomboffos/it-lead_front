import Layout from "../../components/Layout";
import React, { useEffect, useState } from "react";
import Head from "next/head";
import AboutBanner from "../../components/banners/AboutBanner";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft, faArrowRight } from "@fortawesome/free-solid-svg-icons";
import { useDispatch, useSelector } from "react-redux";
import { fetchWorkers } from "../../store/actions/app";
import Loader from "react-loader-spinner";
import CallbackSection from "../../components/section/Callback";

export default function About() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchWorkers());
  }, [dispatch]);

  const workers = useSelector((state) => state.root.app.workers);
  const [currentWorker, setCurrentWorker] = useState(0);

  const addPlus = () => {
    setCurrentWorker(currentWorker + 1);
    if (currentWorker >= workers.length - 1) {
      setCurrentWorker(0);
    }
  };

  const minusPage = () => {
    setCurrentWorker(currentWorker - 1);
    if (currentWorker < 1) {
      setCurrentWorker(workers.length - 1);
    }
  };
  return (
    <Layout banner={<AboutBanner />}>
      <Head>
        <title>О Компании | IT-LEAD</title>
        <meta
          name="description"
          content="Наша Web-студия оказывает комплексный подход по развитию и продвижению вашего бизнеса. На первом этапе мы разрабатываем уникальный сайт с учетом всех пожеланий клиента.
          "
        />
        <link rel="icon" href="/favicon.ico" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
      </Head>
      {workers.length ? (
        <div>
          <div className="about-team-container">
            <div className="about-team-controls">
              {currentWorker + 1}/{workers.length}
              <div className="controls">
                <FontAwesomeIcon
                  icon={faArrowLeft}
                  onClick={() => minusPage()}
                />
                <FontAwesomeIcon
                  icon={faArrowRight}
                  onClick={() => addPlus()}
                />
              </div>
            </div>
            <div className="">
              <div className="positioned-number">{currentWorker + 1}</div>
              <div className="about-team-slider-container">
                <div
                  className="about-team-image"
                  style={{
                    backgroundImage: `url(${workers[currentWorker].image})`,
                  }}
                ></div>
                <div className="about-team-description">
                  {workers[currentWorker].name} <br />{" "}
                  {workers[currentWorker].position}
                </div>
              </div>
            </div>
          </div>
          <CallbackSection />
        </div>
      ) : (
        <div className={"centralized"}>
          <Loader />
        </div>
      )}
    </Layout>
  );
}
