import Head from "next/head";
// import "tailwindcss/tailwind.css";
import Layout from "../components/Layout";
import React, { useEffect, useState } from "react";
import SwiperCore, { Pagination, Navigation } from "swiper/core";
import "swiper/swiper-bundle.css";
import MainBanner from "../components/banners/MainBanner";
import { useDispatch, useSelector } from "react-redux";
import { fetchSettings } from "../store/actions/app";
import { Virtual } from "swiper";
import Advantage from "../components/Advantage/Advantage";
import Projects from "../components/Projects/Projects";
import About from "../components/About/About";
import Partners from "../components/Partners/Partners";
import Service from "../components/Service/Service";
import Contacts from "../components/Contacts/Contacts";

SwiperCore.use([Pagination, Navigation]);

export default function Home() {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchSettings());
  }, [dispatch]);

  const settings = useSelector((state) => state.root.app.settings);

  SwiperCore.use([Virtual]);

  const [slides, setSlides] = useState([]);
  return (
    <div>
      <Head>
        <title>Разработка сайтов и приложений | IT-LEAD</title>
        <meta
          name="description"
          content="Команда профессионалов создаст современные бизнес-инструменты Вашего бренда в онлайн. Разработка сайтов, мобильных приложений и ПО любой сложности и спецификации."
        />
        <link rel="icon" href="/favicon.ico" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link
          rel="preload"
          href="/fonts/SovMod/SovMod.ttf"
          as="font"
          crossOrigin=""
        />
        <link
          rel="preload"
          href="/fonts/Montserrat/Montserrat-Regular.ttf"
          as="font"
          crossOrigin=""
        />
        <link
          rel="preload"
          href="/fonts/Montserrat/Montserrat-Light.woff2Montserrat-Light.ttf"
          as="font"
          crossOrigin=""
        />
      </Head>
      <Layout
        banner={
          <MainBanner
            title={settings["site.description"]}
            subtitle={settings["site.subtitle"]}
          />
        }
      >
        <Advantage title={"ПРЕИМУЩЕСТВА РАБОТЫ С НАМИ"} />
        <Projects title="НАШИ РАБОТЫ" />
        <About title="О КОМПАНИИ" />
        <Partners title="ТЕХНОЛОГИИ" />
        <Service title="Наши услуги" />
        <Contacts title="НАШИ КОНТАКТЫ" />
      </Layout>
    </div>
  );
}
