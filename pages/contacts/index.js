import Layout from "../../components/Layout";
import React from "react";
import Head from "next/head";
import ContactBanner from "../../components/banners/ContactBanner";
import {
  faEnvelope,
  faMailBulk,
  faMapMarker,
  faPhone,
} from "@fortawesome/free-solid-svg-icons";
import { Map, Placemark, YMaps, ZoomControl } from "react-yandex-maps";
import CallbackSection from "../../components/section/Callback";
import ContactBlock from "../../components/ContactBlock/ContactBlock";

export default function Contacts() {
  return (
    <Layout banner={<ContactBanner />}>
      <Head>
        <title>Наши Контакты | IT-LEAD</title>
        <meta name="description" content="Способы связи с нами." />
        <link rel="icon" href="/favicon.ico" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
      </Head>
      <div className="contacts-main-container">
        <div className="contacts-info">
          <ContactBlock
            title={"Телефон"}
            icon={faPhone}
            subtitle={"+7 (707) 303-99-17"}
          />
          <ContactBlock
            title={"Адрес"}
            icon={faMapMarker}
            subtitle={"Байзакова 125"}
          />
          <ContactBlock
            title={"Почта"}
            icon={faEnvelope}
            subtitle={"infoitlead@gmail.com"}
          />
        </div>
        <div className="map-container">
          <YMaps>
            <Map
              defaultState={{ center: [43.256417, 76.910673], zoom: 16 }}
              width={"100%"}
              height={"100%"}
            >
              <Placemark geometry={[43.256417, 76.910673]} />
              <ZoomControl options={{ float: "right" }} />
            </Map>
          </YMaps>
        </div>
      </div>
      <CallbackSection />
    </Layout>
  );
}
