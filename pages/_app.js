import "@fortawesome/fontawesome-svg-core/styles.css"; // import Font Awesome CSS
import { config } from "@fortawesome/fontawesome-svg-core";

config.autoAddCss = false;
import '../styles/globals.css'
import {Provider} from "react-redux";
import store from "../store/store";

function MyApp({Component, pageProps}) {
    return <Provider store={store}><Component {...pageProps} /></Provider>
}

export default MyApp
