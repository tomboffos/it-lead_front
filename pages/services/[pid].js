import Layout from "../../components/Layout";
import React, {useEffect, useState} from "react";
import {useRouter} from "next/router";
import {useDispatch, useSelector} from "react-redux";
import {fetchService} from "../../store/actions/app";
import ServiceBanner from "../../components/banners/ServiceBanner";
import CallbackSection from "../../components/section/Callback";

export default function ServiceShow(props){
    const router = useRouter()
    const {pid} = router.query
    const dispatch = useDispatch()
    const [checked,setChecked] = useState(1)
    useEffect(()=>{
        dispatch(fetchService(pid))
    },[pid,dispatch])

    const service = useSelector((state=>state.root.app.service))
    return (
        <Layout banner={<ServiceBanner service={service}/>}>
            <section className={'properties-container'}>
                <div className="properties-titles">
                    <div className={`property-title ${checked === 1 ? 'active' : ''}`} onClick={()=>setChecked(1)}>описание</div>
                    <div className={`property-title ${checked === 2 ? 'active' : ''}`} onClick={()=>setChecked(2)}>Что входит в работу</div>
                    <div className={`property-title ${checked === 3 ? 'active' : ''}`} onClick={()=>setChecked(3)}>Дополнительные услуги</div>
                </div>
                {checked === 1 ? <div dangerouslySetInnerHTML={{__html:service.description}}/> : ''}
                {checked === 2 ? <div dangerouslySetInnerHTML={{__html : service.work}}/> : ''}
                {checked === 3 ? <div dangerouslySetInnerHTML={{__html :service.additional}}/> : ''}

            </section>
            <CallbackSection/>
        </Layout>
    )
}