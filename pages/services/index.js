import Layout from "../../components/Layout";
import React, { useEffect } from "react";
import Head from "next/head";
import ServicesBanner from "../../components/banners/ServicesBanner";
import SwiperCore, { Pagination, Navigation } from "swiper/core";
import { useDispatch } from "react-redux";
import { fetchCategories, fetchSolutions } from "../../store/actions/app";
import { Virtual } from "swiper";
import "swiper/swiper-bundle.css";
import { faCheck } from "@fortawesome/free-solid-svg-icons";
import CallbackSection from "../../components/section/Callback";
import FastServices from "../../components/FastServices/FastServices";
import Categories from "../../components/Categories/Categories";

SwiperCore.use([Pagination, Navigation]);

export default function Services() {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchCategories());
    dispatch(fetchSolutions());
  }, []);
  SwiperCore.use([Virtual]);
  return (
    <Layout banner={<ServicesBanner />}>
      <Head>
        <title>Наши услуги | IT-LEAD</title>
        <meta
          name="description"
          content="Список услуг по разработке сайтов и приложений."
        />
        <link rel="icon" href="/favicon.ico" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
      </Head>
      <Categories />
      <FastServices
        title="Быстрое решение"
        description={
          "Стоимость разработки индивидуального сайта с базовым функционалом"
        }
        icon={faCheck}
      />
      <CallbackSection />
    </Layout>
  );
}
