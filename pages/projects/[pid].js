import React, { useState, useEffect } from "react";
import Layout from "../../components/Layout";
import ProjectBanner from "../../components/banners/ProjectBanner";
import { useRouter } from "next/router";
import { fetchProject } from "../../store/actions/app";
import { useDispatch, useSelector } from "react-redux";
import Loader from "react-loader-spinner";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft, faArrowRight } from "@fortawesome/free-solid-svg-icons";

export default function Project(props) {
  const router = useRouter();
  const dispatch = useDispatch();
  const { pid } = router.query;
  const [currentImage, setCurrentImage] = useState(1);
  useEffect(() => {
    dispatch(fetchProject(pid));
  }, [pid]);
  const project = useSelector((state) => state.root.app.project);

  const addPage = () => {
    if (currentImage <= project.images.length - 1)
      setCurrentImage(currentImage + 1);
    else setCurrentImage(1);
  };

  const minusPage = () => {
    if (currentImage - 1 === 0) setCurrentImage(project.images.length);
    else setCurrentImage(currentImage - 1);
  };

  return (
    <Layout banner={<ProjectBanner />}>
      {Object.keys(project).length !== 0 ? (
        <div className={"project-container-main"}>
          <div className="project-slider">
            <div className="project-slider-info">
              <div className="project-title project-link">
                <a href={project.link}>{project.link}</a>
              </div>
              <div className="project-title">{project.name}</div>
              <div className="project-main-flex">
                <div className="project-slider-settings">
                  {currentImage}/{project.images.length}
                </div>
                <FontAwesomeIcon
                  icon={faArrowLeft}
                  onClick={() => minusPage()}
                />
                <FontAwesomeIcon
                  icon={faArrowRight}
                  onClick={() => addPage()}
                />
              </div>
            </div>
            <div
              className={`project-images ${!project.web ? "mobile" : ""}`}
              style={{
                backgroundImage: `url(${project.images[currentImage - 1]})`,
              }}
            ></div>
          </div>
          <div className="project-information">
            <div className="idea-container">
              <div className="main-title">Идея</div>
              <div className="main-description">{project.idea}</div>
            </div>
            <div className="other-containers">
              <div className="work-container">
                <div className="main-title">Работа</div>
                <div className="main-description">{project.work}</div>
              </div>
              <div className="realization-container">
                <div className="main-title">Реализация</div>
                <div className="main-description">{project.realization}</div>
              </div>
            </div>
          </div>
          <button className="btn btn-callback" onClick={() => router.back()}>
            Назад
          </button>
        </div>
      ) : (
        <div className={"centralized"}>
          <Loader />
        </div>
      )}
    </Layout>
  );
}
