import Layout from "../../components/Layout";
import React, { useEffect, useState } from "react";
import Head from "next/head";
import ProjectBanner from "../../components/banners/ProjectBanner";
import { useDispatch, useSelector } from "react-redux";
import { fetchProjectCategories } from "../../store/actions/app";
import Loader from "react-loader-spinner";
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation, Pagination, Virtual } from "swiper";
import "swiper/swiper-bundle.css";
import SwiperCore from "swiper/core";
import Link from "next/link";
import CallbackSection from "../../components/section/Callback";
SwiperCore.use([Pagination, Navigation]);

export default function ProjectIndex() {
  const [activeCategory, setActiveCategory] = useState(0);
  const dispatch = useDispatch();
  SwiperCore.use(Virtual);
  useEffect(() => {
    dispatch(fetchProjectCategories());
  }, []);

  const categories = useSelector((state) => state.root.app.projectCategories);
  return (
    <Layout banner={<ProjectBanner />}>
      <Head>
        <title>Наши работы | IT-LEAD</title>
        <meta
          name="description"
          content="Список проектов по разработке сайтов и приложений, созданных нашей командой"
        />
        <link rel="icon" href="/favicon.ico" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
      </Head>
      {categories.length ? (
        <div>
          <div className="projects-index-container">
            <div className="project-category-container">
              {categories.map((category, index) => (
                <div
                  key={category.id}
                  className={`project-category ${
                    activeCategory === index ? "active" : ""
                  }`}
                  onClick={() => setActiveCategory(index)}
                >
                  {category.name}
                </div>
              ))}
            </div>
            <div className="project-show-container">
              <div className="project-bordered-container">
                <Swiper
                  navigation={true}
                  breakpoints={{
                    0: {
                      slidesPerView: 1,
                    },
                    // when window width is >= 768px
                  }}
                  className="mySwiper"
                  virtual
                >
                  {categories[activeCategory].projects.map((project) => (
                    <SwiperSlide virtualIndex={project.id} key={project.id}>
                      <Link href={`/projects/${project.id}`}>
                        <div className="project-main-container">
                          <img src={project.logo} alt="" />
                        </div>
                      </Link>
                    </SwiperSlide>
                  ))}
                </Swiper>
              </div>
            </div>
          </div>
          <CallbackSection />
        </div>
      ) : (
        <div className={"centralized"}>
          <Loader />
        </div>
      )}
    </Layout>
  );
}
