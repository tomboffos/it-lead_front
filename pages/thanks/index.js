import React from "react";
import router from "next/router";
import Layout from "../../components/Layout";
import {
  faEnvelope,
  faMailBulk,
  faMapMarker,
  faPhone,
} from "@fortawesome/free-solid-svg-icons";
import { Map, Placemark, YMaps, ZoomControl } from "react-yandex-maps";
import CallbackSection from "../../components/section/Callback";
import ContactBlock from "../../components/ContactBlock/ContactBlock";

const thank = () => {
  return (
    <Layout>
      <div className="thanks">
        <div className="thanks-message">
          <h2>Спасибо за заявку!</h2>
          <p>В ближайшее время мы с вами свяжемся.</p>
          <button className="btn" onClick={() => router.back()}>
            Назад
          </button>
        </div>
      </div>
    </Layout>
  );
};

export default thank;
