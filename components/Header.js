import React, { useEffect, useState } from "react";
import Link from "next/link";
import MainBanner from "./banners/MainBanner";
import Head from "next/head";
import Swal from "sweetalert2";
import Modal from "react-modal";
import InputMask from "react-input-mask";
import { useDispatch } from "react-redux";
import { sendApplication } from "../store/actions/app";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars, faPhone } from "@fortawesome/free-solid-svg-icons";
import { faTimes } from "@fortawesome/free-solid-svg-icons/faTimes";
import router from "next/router";
import Image from "next/image";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    background: "#292C35",
    border: "none",
    width: "90%",
    borderRadius: "25px",
  },
};
export default function Header(props) {
  const [scrolled, setScrolled] = useState(false);
  const handleScroll = () => {
    const offset = window.scrollY;

    if (offset > 200) {
      setScrolled(true);
    } else {
      setScrolled(false);
    }
  };
  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
  });

  const [show, setShow] = useState(false);
  const [form, setForm] = useState({
    name: "",
    phone: "",
    comments: "",
  });
  const [menu, setMenu] = useState(false);
  const dispatch = useDispatch();
  const changeHandler = (event) =>
    setForm({ ...form, [event.target.name]: event.target.value });
  const handleLinkClick = (e) => {
    setMenu(false);
  };
  return (
    <div className={"main-header-component"}>
      <header className={`header main ${scrolled ? "sticky" : ""}`}>
        <div className="menu-bar" onClick={() => setMenu(!menu)}>
          {menu ? (
            <FontAwesomeIcon icon={faTimes} />
          ) : (
            <FontAwesomeIcon icon={faBars} />
          )}
        </div>
        <Link href={"/"}>
          <div className="logo-thumbnail">
            <Image
              src="/logo.png"
              alt="IT-LEAD"
              width={100}
              height={70}
              objectFit={true}
            />
          </div>
        </Link>
        <div className={`navbar-container ${menu ? "active" : ""}`}>
          <div className={`navbar ${menu ? "active" : ""}`}>
            <li>
              <Link href={"/#"} onClick={() => setMenu(false)}>
                Главная
              </Link>
            </li>
            <li>
              <Link href={"/projects"} onClick={() => setMenu(false)}>
                Портфолио
              </Link>
            </li>
            <li>
              <Link href={"/services"} onClick={() => setMenu(false)}>
                Услуги
              </Link>
            </li>
            <li>
              <Link href={"/about"} onClick={() => setMenu(false)}>
                О нас
              </Link>
            </li>
            <li>
              <Link href={"/contacts"} onClick={() => setMenu(false)}>
                Контакты
              </Link>
            </li>
            {menu ? (
              <li>
                <a href="tel:+77073039917">+7 (707) 303-99-17</a>
              </li>
            ) : (
              ""
            )}
          </div>
        </div>
        <button
          className="main-theme-button callback-button"
          onClick={() => setShow(true)}
        >
          Заказать звонок
        </button>
        <div className="insert-number-container">
          <FontAwesomeIcon icon={faPhone} />
          <a href="tel:+77073039917">
            <div className="insert-number-field">+7 (707) 303-99-17</div>
          </a>
        </div>
      </header>
      {props.children}
      <div
        className={`menu-bar-overlay ${menu ? "active" : ""}`}
        onClick={() => setMenu(false)}
      />

      <Modal
        isOpen={show}
        className={"modal-callback"}
        onRequestClose={() => setShow(false)}
      >
        <div className="modal-main">
          <div className="modal-header">
            <div className="modal-title">Оставьте нам заявку</div>
            <i className="fas fa-times" onClick={() => setShow(false)} />
          </div>
          <div className="form-control">
            <input
              type="text"
              name={"name"}
              placeholder={"Имя"}
              onChange={changeHandler}
              value={form.name}
            />
          </div>
          <div className="form-control">
            <InputMask
              mask={"+9 (999) 999-99-99"}
              onChange={changeHandler}
              value={form.phone}
            >
              {(inputProps) => (
                <input
                  type="tel"
                  name={"phone"}
                  placeholder={"Номер телефона"}
                />
              )}
            </InputMask>
          </div>
          <div className="form-control">
            <textarea
              name="comments"
              id=""
              rows="4"
              onChange={changeHandler}
              placeholder={"Комментарии"}
            >
              {form.comments}
            </textarea>
          </div>
          <button
            className="callback-button"
            onClick={() => {
              dispatch(sendApplication(form));
              setShow(false);
              setForm({ ...form, name: "" });
              setForm({ ...form, phone: "" });
              setForm({ ...form, comments: "" });
              router.push("/thanks");
            }}
          >
            Оставить заявку
          </button>
        </div>
      </Modal>
    </div>
  );
}
