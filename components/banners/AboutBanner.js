import {useDispatch, useSelector} from "react-redux";
import {useEffect} from "react";
import {fetchSettings} from "../../store/actions/app";

export default function AboutBanner() {
    const dispatch = useDispatch()
    useEffect(()=>{
        dispatch(fetchSettings())

    },[])
    const settings = useSelector(state => state.root.app.settings)

    return (
        <div className={'banner-container'}>
            <div
                style={{backgroundImage:'url(/banners/about-banner.png)'}}
                className="background-banner-container project-banner-container service-banner-container service-new-container">
                <div className="positioned-text-project">
                    Наша команда
                </div>
                <div className="title-text">
                    О нас
                </div>
                <div className="description-banner">
                    {settings['site.about_page_title']}
                </div>

            </div>
        </div>
    )
}