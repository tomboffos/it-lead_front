import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faWhatsapp } from "@fortawesome/free-brands-svg-icons/faWhatsapp";
import { faTelegram } from "@fortawesome/free-brands-svg-icons";

export default function MainBanner(props) {
  const [text, setText] = useState("");
  let programmingText =
    '(self.webpackChunk_N_E=self.webpackChunk_N_E||[]).push([[179],{7228:function(e){e.exports=function(e,t){(null==t||t>e.length)&&(t=e.length);for(var r=0,n=new Array(t);r<t;r++)n[r]=e[r];return n}},2858:function(e){e.exports=function(e){if(Array.isArray(e))return e}},1506:function(e){e.exports=function(e){if(void 0===e)throw new ReferenceError("this hasn\'t been initialised - super() hasn\'t been called");return e}},8926:function(e){function t(e,t,r,n,o,a,i){try{var u=e[a](i),c=u.value}catch(s){return void r(s)}u.done?t(c):Promise.resolve(c).then(n,o)}e.exports=function(e){return function(){var r=this,n=arguments;return new Promise((function(o,a){var i=e.apply(r,n);function u(e){t(i,o,a,u,c,"next",e)}function c(e){t(i,o,a,u,c,"throw",e)}u(void 0)}))}}},4575:function(e){e.exports=function(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}},9100:function(e,t,r){var n=r(9489),o=r(7067);function a(t,r,i){return o()?e.exports=a=Reflect.construct:e.exports=a=function(e,t,r){var o=[null];o.push.apply(o,t);var a=new(Function.bind.apply(e,o));return r&&n(a,r.prototype),a},a.apply(null,arguments)}e.exports=a},3913:function(e){function t(e,t){for(var r=0;r<t.length;r++){var n=t[r];n.enumerable=n.enumerable||!1,n.configurable=!0,"value"in n&&(n.writable=!0),Object.defineProperty(e,n.key,n)}}e.exports=function(e,r,n){return r&&t(e.prototype,r),n&&t(e,n),e}},7154:function(e){function t(){return e.exports=t=Object.assign||function(e){for(var t=1;t<arguments.length;t++){var r=arguments[t];for(var n in r)Object.prototype.hasOwnProperty.call(r,n)&&(e[n]=r[n])}return e},t.apply(this,arguments)}e.exports=t},9754:function(e){function t(r){return e.exports=t=Object.setPrototypeOf?Object.getPrototypeOf:function(e){return e.__proto__||Object.getPrototypeOf(e)},t(r)}e.exports=t},2205:function(e,t,r){var n=r(9489);e.exports=function(e,t){if("function"!==typeof t&&null!==t)throw new TypeError("Super expression must either be null or a function");e.prototype=Object.create(t&&t.prototype,{constructor:{value:e,writable:!0,configurable:!0}}),t&&n(e,t)}},5318:function(e){e.exports=function(e)';
  let intervalText = "";
  useEffect(() => {
    let i = 0;
    setInterval(() => {
      intervalText = intervalText + programmingText[i];
      setText(intervalText);
      i++;
    }, 250);
  }, [programmingText]);

  return (
    <div className={"banner-container"}>
      {/*<div className="positioned-text">*/}
      {/*    Соз<span>даем</span>*/}
      {/*</div>*/}
      <div className="background-banner-container">
        <div className="">
          <div className="programming-text">{text}</div>

          <div className="banner-main-text">{props.title}</div>
          <div className="banner-subtitle">{props.subtitle}</div>
        </div>
        <div className="positioned-social-icons">
          <div className="line-up" />
          <div className="social-icons">
            {/*<i className="fab fa-instagram"/>*/}
            {/*<i className="fab fa-facebook-square"/>*/}
            <a
              target="_blank"
              rel="noreferrer"
              href="https://telegram.me/tomboffos"
            >
              <FontAwesomeIcon icon={faTelegram} />
            </a>
            <a
              target="_blank"
              rel="noreferrer"
              href="https://api.whatsapp.com/send/?phone=77073039917&text=Здравствуйте%2C+у+меня+есть+вопрос"
            >
              <FontAwesomeIcon icon={faWhatsapp} />
            </a>
          </div>
          <div className="line-down" />
        </div>
      </div>
    </div>
  );
}
