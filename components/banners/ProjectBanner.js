export default function ProjectBanner(props) {
    return (
        <div className={'banner-container'}>
            <div className="background-banner-container project-banner-container">
                <div className="positioned-text-project">
                    ПОРТФОЛИО
                </div>
                <div className="title-project">
                    НАШИ <span>РАБОТЫ</span>
                </div>
            </div>
        </div>
    )
}