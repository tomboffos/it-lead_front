export default function ContactBanner() {
    return (
        <div className={'banner-container'}>
            <div
                style={{backgroundImage:'url(/banners/contacts-banner.png)'}}
                className="background-inner-container
                contacts-banner
                 project-banner-container service-banner-container service-new-container">
                <div className="positioned-text-project">
                    Связь с нами
                </div>
                <div className="title-text">
                    контакты
                </div>
            </div>
        </div>
    )
}