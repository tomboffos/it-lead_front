import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft, faArrowRight } from "@fortawesome/free-solid-svg-icons";
import Modal from "react-modal";
import InputMask from "react-input-mask";
import { sendApplication } from "../../store/actions/app";
import { useDispatch } from "react-redux";

export default function ServiceBanner(props) {
  const [currentImage, setCurrentImage] = useState(0);
  const minusPage = () => {
    setCurrentImage(currentImage - 1);
    if (currentImage < 1) {
      setCurrentImage(props.service.images.length - 1);
    }
  };
  const addPage = () => {
    setCurrentImage(currentImage + 1);
    if (currentImage >= props.service.images.length - 1) {
      setCurrentImage(0);
    }
  };
  const dispatch = useDispatch();
  const changeHandler = (event) =>
    setForm({ ...form, [event.target.name]: event.target.value });
  const [form, setForm] = useState({
    name: "",
    phone: "",
    comments: "",
  });
  const [show, setShow] = useState(false);

  return (
    <div className={"banner-container"}>
      <div className="background-banner-container project-banner-container service-banner-container service-new-container">
        <div className="positioned-text-project">Название сайта</div>
        <div className="title-text">{props.service.name}</div>
        <div className="site-start-information">
          {props.service.images != null ? (
            <div className="images-slider">
              <div
                className="slider-thumbnail"
                style={{
                  backgroundImage: `url(${props.service.images[currentImage]})`,
                  backgroundSize: "cover",
                }}
              >
                {" "}
              </div>

              <div className="slider-controls">
                {currentImage + 1}/{props.service.images.length}
                <div className="controls">
                  <FontAwesomeIcon
                    icon={faArrowLeft}
                    onClick={() => minusPage()}
                  />
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    onClick={() => addPage()}
                  />
                </div>
              </div>
            </div>
          ) : (
            ""
          )}
          <div className="site-information-short">
            <div className="price">Цена {props.service.price} тг</div>
            <div className="short-info">{props.service.short_description}</div>
            <button className="demo">Демо-версия</button>
            <div className="">
              <button className="order-now" onClick={() => setShow(true)}>
                Заказать сайт у нас
              </button>
            </div>
          </div>
        </div>
      </div>
      <Modal
        isOpen={show}
        className={"modal-callback"}
        onRequestClose={() => setShow(false)}
      >
        <div className="modal-main">
          <div className="modal-header">
            <div className="modal-title">Оставьте нам заявку</div>
            <i className="fas fa-times" onClick={() => setShow(false)} />
          </div>
          <div className="form-control">
            <input
              type="text"
              name={"name"}
              placeholder={"Имя"}
              onChange={changeHandler}
              value={form.name}
            />
          </div>
          <div className="form-control">
            <InputMask
              mask={"+9 (999) 999-99-99"}
              onChange={changeHandler}
              value={form.phone}
            >
              {(inputProps) => (
                <input
                  type="tel"
                  name={"phone"}
                  placeholder={"Номер телефона"}
                />
              )}
            </InputMask>
          </div>
          <div className="form-control">
            <textarea
              name="comments"
              id=""
              rows="4"
              onChange={changeHandler}
              placeholder={"Комментарии"}
            >
              {form.comments}
            </textarea>
          </div>
          <button
            className="callback-button"
            onClick={() => {
              dispatch(sendApplication(form));
              setShow(false);
              setForm({ ...form, name: "" });
              setForm({ ...form, phone: "" });
              setForm({ ...form, comments: "" });
            }}
          >
            Оставить заявку
          </button>
        </div>
      </Modal>
    </div>
  );
}
