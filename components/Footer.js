import React from "react";
import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPhone } from "@fortawesome/free-solid-svg-icons/faPhone";
import { faEnvelope, faMapMarker } from "@fortawesome/free-solid-svg-icons";
import { faTelegram, faWhatsapp } from "@fortawesome/free-brands-svg-icons";

export default function Footer() {
  return (
    <footer className={"footer-container"} id={"footer"}>
      <img src="/logo.png" alt="" className={"logo"} />
      <hr />
      <div className="d-flex">
        <div className="contacts-block">
          <a href="tel:+77073039917">
            <div className="icon-text">
              <FontAwesomeIcon icon={faPhone} />
              <div>+7(707)303-99-17</div>
            </div>
          </a>
          <a href="https://go.2gis.com/wvupm" target="_blank" rel="noreferrer">
            <div className="icon-text">
              <FontAwesomeIcon icon={faMapMarker} />
              <div>Байзакова 125</div>
            </div>
          </a>

          <a href="mailto:infoitlead@gmail.com">
            <div className="icon-text">
              <FontAwesomeIcon icon={faEnvelope} />
              <div>infoitlead@gmail.com</div>
            </div>
          </a>
        </div>
        <div className="links-block">
          <div>
            <Link href={"/#"}>Главная</Link>
          </div>
          <div>
            <Link href={"/#projects"}>Портфолио</Link>
          </div>
          {/*<div><Link href={'/'}>Услуги</Link></div>*/}
          <div>
            <Link href={"/#about"}>О нас</Link>
          </div>
          <div>
            <Link href={"/#footer"}>Контакты</Link>
          </div>
        </div>
        <div className="rights-block">
          <div>
            <Link href={"/"} target="_blank" rel="noreferrer">
              Политика конфиденциальности
            </Link>
          </div>
          <div>
            <Link href={"/"} target="_blank" rel="noreferrer">
              Социальная ответственность
            </Link>
          </div>
        </div>
        <div className="social-links-block">
          {/*<div>*/}
          {/*    <a href="">*/}
          {/*        <i className="fab fa-instagram"/>*/}
          {/*    </a>*/}
          {/*</div>*/}
          {/*<div>*/}
          {/*    <a href="">*/}
          {/*        <i className="fab fa-facebook-square"/>*/}
          {/*    </a>*/}
          {/*</div>*/}
          <div>
            <a href="https://telegram.me/tomboffos">
              <FontAwesomeIcon icon={faTelegram} />
            </a>
          </div>
          <div>
            <a href="https://api.whatsapp.com/send/?phone=77073039917&text=Здравствуйте%2C+у+меня+есть+вопрос">
              <FontAwesomeIcon icon={faWhatsapp} />
            </a>
          </div>
        </div>
      </div>
    </footer>
  );
}
