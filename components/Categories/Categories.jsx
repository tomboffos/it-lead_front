import React, { useEffect } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, { Pagination, Navigation } from "swiper/core";
import Link from "next/link";
import { useDispatch, useSelector } from "react-redux";
import { fetchCategories } from "../../store/actions/app";
const Categories = () => {
  const categories = useSelector((state) => state.root.app.categories);
  SwiperCore.use([Pagination, Navigation]);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchCategories());
  }, [dispatch]);
  return (
    <div className="fast-solution-container">
      {categories.map((category) => (
        <div className={"category-box"} key={category.id}>
          <div className="category-title">{category.name}</div>
          <div className="category-slider">
            <Swiper
              navigation={true}
              breakpoints={{
                0: {
                  slidesPerView: 1,
                },
                // when window width is >= 768px
                720: {
                  slidesPerView: 3,
                },
              }}
              className="mySwiper"
              virtual
            >
              {category.services.map((service, index) => (
                <SwiperSlide virtualIndex={index} key={service.id}>
                  <Link href={`/services/${service.slug}`}>
                    <div className="main-category-service-box">
                      <div className="category-service-box">
                        <div className="service-foreground-box">
                          <div
                            className="category-service-image"
                            style={{
                              backgroundImage: `url(${service.image})`,
                            }}
                          />
                          <div className="category-service-title">
                            {service.name}
                          </div>
                          <div className="category-service-price">
                            {service.price} тг
                          </div>
                        </div>
                      </div>
                    </div>
                  </Link>
                </SwiperSlide>
              ))}
            </Swiper>
          </div>
        </div>
      ))}
    </div>
  );
};

export default Categories;
