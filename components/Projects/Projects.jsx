import React, { useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, { Virtual, Autoplay } from "swiper";
import ProjectItem from "../../components/items/ProjectItem";
import { useSelector } from "react-redux";
import "swiper/swiper.min.css";

const Projects = ({ title }) => {
  SwiperCore.use([Virtual, Autoplay]);
  const projects = useSelector((state) => state.root.app.projects);
  return (
    <div className="projects-container">
      <div className="projects-overlay">
        <div className="projects-title-container">
          <span className="projects-title-back-word">Портфолио</span>
          <h2 className="projects-title">{title}</h2>
        </div>
        <Swiper
          autoplay={{ delay: 2000, disableOnInteraction: false }}
          disable
          // speed={5000}
          navigation={true}
          virtual
          observer
          // centeredSlides
          // loop
          // centeredSlides
          breakpoints={{
            0: {
              slidesPerView: 1,
            },
            // when window width is >= 768px
            720: {
              slidesPerView: 4,
            },
          }}
          spaceBetween={5}
          className="mySwiper"
        >
          {projects.map((project, index) => (
            <SwiperSlide virtualIndex={index} key={project.id}>
              <ProjectItem project={project} key={project.id} />
            </SwiperSlide>
          ))}
        </Swiper>
      </div>
    </div>
  );
};

export default Projects;
