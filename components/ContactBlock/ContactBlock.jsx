import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const ContactBlock = ({ icon, title, subtitle }) => {
  return (
    <div className="contact-block">
      <div className="icon-dotted-border">
        <div className="icon-container">
          <FontAwesomeIcon icon={icon} />
        </div>
      </div>
      <div className="contact-info">
        <div className="title-contact">{title}</div>
        <div className="subtitle-icon">{subtitle}</div>
      </div>
    </div>
  );
};

export default ContactBlock;
