import React, { useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useDispatch, useSelector } from "react-redux";
import { fetchSolutions } from "../../store/actions/app";

const FastServices = ({ title, description, icon }) => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchSolutions());
  }, [dispatch]);

  const solutions = useSelector((state) => state.root.app.fastSolutions);
  return (
    <section className="fast-services-container">
      <h2 className="title-fast">{title}</h2>
      {solutions.map((solution, index) => (
        <div className="fast-solution-box" key={solution.id}>
          <div className="fact-price-container">
            <div className="fast-solution-number">{index + 1}</div>
            <div className="fast-solution-title">{solution.name}</div>
            <div className="fast-solution-description">
              {solution.short_description}
            </div>
            <div className="fact-price">
              <s>{solution.fact_price.toLocaleString()} тг</s>
            </div>
            <div className="function-price">{description}</div>
          </div>
          <div className="actual-price">
            <div className="check-point">
              <FontAwesomeIcon icon={icon} />
            </div>
            <div className="actual-price-title">
              от {solution.price.toLocaleString()} тг в{" "}
              {(solution.fact_price / solution.price).toFixed(2)} раза выгоднее
            </div>
            <div className="post-title">
              Стоимость создания сайта каталога на готовом решении
            </div>
          </div>
        </div>
      ))}
    </section>
  );
};

export default FastServices;
