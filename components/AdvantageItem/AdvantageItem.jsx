import React from "react";

const AdvantageItem = ({ name, descr }) => {
  return (
    <div className="advantage_container">
      <div className="advantage_name">{name}</div>
      <div className="advantage_linear" />
      <div className="advantage_description">{descr}</div>
    </div>
  );
};

export default AdvantageItem;
