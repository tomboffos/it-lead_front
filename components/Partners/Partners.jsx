import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { useSelector } from "react-redux";
import SwiperCore, { Virtual, Autoplay } from "swiper";
import "swiper/swiper.min.css";
import Image from "next/image";

const Partners = ({ title }) => {
  SwiperCore.use([Virtual, Autoplay]);
  const technologies = useSelector((state) => state.root.app.technologies);

  return (
    <div className="partners-container">
      <span className="positioned-title-back-word">Мы используем</span>
      <h2 className="partners-title">{title}</h2>
      <div className="partners-slider">
        <Swiper
          navigation={true}
          breakpoints={{
            0: {
              slidesPerView: 1,
            },
            // when window width is >= 768px
            720: {
              slidesPerView: 4,
            },
          }}
          spaceBetween={50}
          className="mySwiper"
          virtual
          autoplay
        >
          {technologies.map((technology, index) => (
            <SwiperSlide virtualIndex={index} key={technology.id}>
              <div className={"slider-image"}>
                <img src={technology.logo} alt="" />
              </div>
            </SwiperSlide>
          ))}
        </Swiper>
      </div>
      <div id={"services"} />
    </div>
  );
};

export default Partners;
