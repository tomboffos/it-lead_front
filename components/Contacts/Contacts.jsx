import React, { useState } from "react";
import FormInput from "../FormInput/FormInput";
import InputMask from "react-input-mask";
import { useDispatch } from "react-redux";
import { sendApplication } from "../../store/actions/app";
import Image from "next/image";

const Contacts = ({ title }) => {
  const [form, setForm] = useState({
    name: "",
    phone: "",
    comments: "",
  });
  const dispatch = useDispatch();
  const changeHandler = (event) =>
    setForm({ ...form, [event.target.name]: event.target.value });
  return (
    <div className="contacts-section">
      <span className="callback-title-back-word">ОБРАТНАЯ СВЯЗЬ</span>
      <div className="ellipse-background"></div>
      <div className="linear-main" />
      <h2 className="contacts-title">{title}</h2>
      <div className="d-flex">
        <div className="img-contacts">
          <Image src="/team.webp" alt={title} width={483} height={340} />
        </div>
        <div className="text-contacts">
          <FormInput
            type="text"
            name="name"
            placeholder="Введите имя"
            onChange={changeHandler}
            defaultValue={form.name}
          />
          <div className="form-control">
            <InputMask
              mask={"+9 (999) 999-99-99"}
              onChange={changeHandler}
              defaultValue={form.phone}
            >
              {(inputProps) => (
                <input
                  type="tel"
                  placeholder={"Введите номер"}
                  name={"phone"}
                />
              )}
            </InputMask>
          </div>
          <FormInput
            rows={6}
            placeholder="Описание вопроса"
            name="comments"
            onChange={changeHandler}
            textarea
            value={form.comments}
          ></FormInput>

          <button
            className={"callback-button"}
            onClick={() => {
              dispatch(sendApplication(form));
              setForm({ ...form, name: "" });
              setForm({ ...form, phone: "" });
              setForm({ ...form, comments: "" });
            }}
          >
            Оставить заявку
          </button>
        </div>
      </div>
    </div>
  );
};

export default Contacts;
