import React from "react";
import ServiceItem from "../ServiceItem/ServiceItem";

const Service = ({ title }) => {
  return (
    <div className="service-container">
      <h2 className="title-services">{title}</h2>
      <div className="container-services">
        <ServiceItem name="Мобильные приложения" descr={"site.app"} />
        <ServiceItem name="Сайты" descr={"site.site"} />
        <ServiceItem name="ПО" descr={"site.programs"} />
      </div>
    </div>
  );
};

export default Service;
