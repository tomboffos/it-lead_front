import InputMask from "react-input-mask";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { sendApplication } from "../../store/actions/app";

export default function CallbackSection() {
  const [form, setForm] = useState({
    name: "",
    phone: "",
    comments: "",
  });
  const dispatch = useDispatch();

  const changeHandler = (event) =>
    setForm({ ...form, [event.target.name]: event.target.value });
  return (
    <div className={"callback-section-main"}>
      <div className="forms-container">
        <div className="form-control-new">
          <input
            type="text"
            placeholder={"Название"}
            value={form.name}
            name={"name"}
            onChange={changeHandler}
          />
        </div>
        <div className="form-control-new">
          <InputMask
            mask={"+9 (999) 999-99-99"}
            onChange={changeHandler}
            value={form.phone}
          >
            {(inputProps) => (
              <input type="tel" placeholder={"Телефон"} name={"phone"} />
            )}
          </InputMask>
        </div>
      </div>
      <div className="forms-container">
        <textarea
          name="comments"
          id=""
          cols="30"
          rows="6"
          placeholder={"Комментарии"}
          onChange={changeHandler}
        >
          {form.comments}
        </textarea>
      </div>
      <div className="callback">
        <button
          className={"callback-button-new"}
          onClick={() => {
            dispatch(sendApplication(form));
            setForm({ ...form, name: "" });
            setForm({ ...form, phone: "" });
            setForm({ ...form, comments: "" });
          }}
        >
          Оставить заявку
        </button>
      </div>
    </div>
  );
}
