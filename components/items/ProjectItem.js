import Link from "next/link";
import React from "react";
import { useDispatch } from "react-redux";

export default function ProjectItem(props) {
  const dispatch = useDispatch();
  return (
    <Link href={`/projects/${props.project.id}`}>
      <div className="box-main">
        <div className="project-category-box">
          <img src={props.project.logo} alt="" />
          <div className="project-category-title">{props.project.name}</div>
        </div>
      </div>
    </Link>
  );
}
