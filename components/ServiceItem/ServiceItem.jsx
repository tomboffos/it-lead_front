import React from "react";
import { useSelector } from "react-redux";

const ServiceItem = ({ name, descr }) => {
  const settings = useSelector((state) => state.root.app.settings);

  return (
    <div className="service-box">
      <div className="title-service">{name}</div>
      <div className="inline-service" />
      <div className="image-service-layout">
        <div className="overlay-gray">{settings[descr]}</div>
      </div>
    </div>
  );
};

export default ServiceItem;
