import React, { useEffect } from "react";
import AdvantageItem from "../AdvantageItem/AdvantageItem";
import { fetchSettings } from "../../store/actions/app";
import { useDispatch, useSelector } from "react-redux";

const Advantage = ({ title }) => {
  const settings = useSelector((state) => state.root.app.settings);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchSettings());
  }, [dispatch]);
  return (
    <div className="advantages-container">
      <div className="advantages-title-container">
        <span className="positioned-main-title">Преимущество</span>

        <div className="main-title-advantages">
          <div className="ellipse-background"></div>
          <div className="linear" />
          <h2 className="title-advantages">{title}</h2>
        </div>
        <div className="centralized-advantages">
          <AdvantageItem name="Сроки" descr={settings["site.deadline"]} />
          <AdvantageItem name="Качество" descr={settings["site.quality"]} />
          <AdvantageItem
            name="Тех.Поддержка"
            descr={settings["site.tech_support"]}
          />
          <AdvantageItem name="Дизайн" descr={settings["site.design"]} />
        </div>
      </div>
    </div>
  );
};

export default Advantage;
