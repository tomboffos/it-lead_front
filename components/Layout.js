import Header from "./Header";
import React from "react";
import Footer from "./Footer";
import MainBanner from "./banners/MainBanner";

export default function Layout(props) {
    return (
        <div>
            <Header>
                {props.banner}
            </Header>
            {props.children}
            <Footer/>
        </div>
    )
}