import React from "react";

const FormInput = ({ changeHandler, textarea, ...otherProps }) => {
  return (
    <div className="form-control">
      {textarea ? (
        <textarea onChange={changeHandler} {...otherProps}></textarea>
      ) : (
        <input onChange={changeHandler} {...otherProps} />
      )}
    </div>
  );
};

export default FormInput;
