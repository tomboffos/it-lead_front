import React, { useEffect } from "react";
import {
  fetchProjects,
  getTechnologies,
  fetchSettings,
} from "../../store/actions/app";
import { useDispatch, useSelector } from "react-redux";
import Image from "next/image";

const About = ({ title }) => {
  const settings = useSelector((state) => state.root.app.settings);

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchProjects());
    dispatch(getTechnologies());
    dispatch(fetchSettings());
  }, [dispatch]);

  return (
    <div className="about-container" id="about">
      <div className="d-flex">
        <div className="about-text">
          <span className="about-title-back-word">О НАС</span>
          <div className="about-front-word">
            <div className="linear" />
            <div className="ellipse-background" />
            <h2 className="about-title">{title}</h2>
          </div>
          <div className="about-text-main">{settings["site.about"]}</div>
        </div>
        <div className="about-image">
          <Image
            src="/team-image.webp"
            alt="Команда"
            width={662}
            height={372}
          />
        </div>
      </div>
    </div>
  );
};

export default About;
