module.exports = {
  siteUrl: process.env.SITE_URL || "https://it-lead.net/",
  generateRobotsTxt: true,
  robotsTxtOptions: {
    policies: [
      { userAgent: "*", disallow: "/thanks" },
      { userAgent: "*", allow: "/" },
    ],
  },
  exclude: ["/thanks"],
};
