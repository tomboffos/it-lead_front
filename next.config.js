module.exports = {
  reactStrictMode: true,
  images: {
    domains: ["backs.it-lead.net"],
  },
  siteUrl: process.env.SITE_URL || "https://it-lead.net/",
  generateRobotsTxt: true, // (optional)
};
